const subBut = document.getElementById('Submit').value;
const scrip = document.createElement('script');
import Sha256 from "./sha.js";


if (subBut == "Sign Up"){
        var firm = document.getElementById('signUpInfo');
        console.log("registration title loaded");

        firm.addEventListener('submit', (e) => {
                var usr = document.getElementById('user').value;
                var pass1 = document.getElementById('password1').value;
                var pass2 = document.getElementById('password2').value;
                if (pass1 != pass2){
                        window.alert("PLEASE MAKE SURE BOTH PASSWORDS MATCH");
                        e.preventDefault();
                }

                if (usr == null || pass1 == null || pass2 == null){
                        window.alert("PLEASE MAKE SURE USERNAME/PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (usr.length > 16 || usr.length < 6){
                        window.alert("PLEASE MAKE SURE USERNAME IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (pass1.length > 16 || pass1.length < 6){
                        window.alert("PLEASE MAKE SURE PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }

                var num = /\D/;
                var alph = /\W/;
                if (num.test(pass1) && alph.test(pass1)){
                        window.alert("PLEASE MAKE SURE PASSWORD CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }

                if (num.test(usr) && alph.test(usr)){
                        window.alert("PLEASE MAKE SURE USERNAME CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }
                document.getElementById('password1').value = Sha256.hash(pass1);
        })

}


if (subBut == "Log in"){ 
        var farm = document.getElementById('loginInfo');
        console.log("login title loaded");

        farm.addEventListener('submit', (e) => {
                var usr = document.getElementById('user').value;
                var pass = document.getElementById('password').value;
                if (usr == null || pass == null){
                        window.alert("PLEASE MAKE SURE USERNAME/PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (usr.length > 16 || usr.length < 6){
                        window.alert("PLEASE MAKE SURE USERNAME IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (pass.length > 16 || pass.length < 6){
                        window.alert("PLEASE MAKE SURE PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }

                var num = /\D/;
                var alph = /\W/;
                if (num.test(pass) && alph.test(pass)){
                        window.alert("PLEASE MAKE SURE PASSWORD CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }

                if (num.test(usr) && alph.test(usr)){
                        window.alert("PLEASE MAKE SURE USERNAME CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }
                document.getElementById('password').value = Sha256.hash(pass);
        })
}
  
