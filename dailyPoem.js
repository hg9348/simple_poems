//js file for poem of the day, for now have it change every 10 seconds


//poem bank for poem of the day, seperated by poem, seperated into stanzas
const bank = [
//poem 1
[["With warts on her nose", "And sharp pointy toes", "She flies through the night on her broom"],["With covers pulled tight","In the shadows of night", "I hide in the dark of my room"]],
//poem 2
[["The clouds passing by", "In the shadows of night", "To another time..."]],
//poem 3
[["Faith says the soothsayer, is a myth", "Discarded even by the most faithful" , "Used only when it suitable to them"]],
//poem 4
[["Yellow, and black, and pale, and hectic red,  Pestilence-stricken multitudes: O thou, Who chariotest to their dark wintry bed", "The winged seeds, where they lie cold and low, Each like a corpse within its grave, until Thine azure sister of the Spring shall blow", "Her clarion o'er the dreaming earth, and fill (Driving sweet buds like flocks to feed in air) With living hues and odours plain and hill:"]],
//poem 5
[["Love between us is", "Speech and breath. Loving you is", "A long river running"]],
//poem 6
[["I come to you in dreams as soft as dew","As sunlight kisses sweet the morning rose","And day break gold and red gives way to blue"], ["Night bird seeks her nest and soft eyes close", "Seeking slumber deep, magic found in dreams", "Exhale life’s toil and care to seek repose"]],
//poem7
[["Be it no more than just a glass of water,", "A live walking stick played by a kind daughter, ", "Care and concern, warm smile, not else nigh hotter; "], ["Or quality time spent with someone old, ", "Blanket’s warm fold in times forlorn and cold, ", "In times of need a ready shoulder-hold; "], ["A pair of slippers to feet walking bare, ", "Not in loud charity to show you care, ", "Heart-born feelings so felt drowning false air."], ["Anything— given short of counting ways, ", "Given to brighten up sinking heart’s greys, ", "To lighten load that too heavily weighs. "], ["Give it in cash if it can’t be in kind, ", "Let it a gift of heart be, well inclined, ", "A gift of very soul, body and mind. "], ["Give— the only joy greater than getting", "Be the joy of giving and forgetting,", "And bending once more O for giving. "]]

];

const titleBank = ["Witch Way", "Clouds", "Faith", "Ode to the West Wind", "Haiku (for you)" , "To Find Rest", "The Joy of Giving"];
const authorBank = ["Charles Ghigna", "Jesus Diaz Llorico", "Ravikiran Arakkal" , "Percy Bysshe Shelley","Sonia Sanchez", "Steve Waldrop" , "Aniriddhu Pathak"];

//get date
var today = new Date();
var day = today.getDay();




const date = document.querySelector('#time');
var poBox = document.querySelector('#poemBox');

const poem = bank[day];
const stanzas = poem.length;

title(day, titleBank, poBox);
construction(stanzas, poem, poBox);
author(day, authorBank, poBox);




function construction(stanzas, poem, poBox){
      var lines = new Array();
      var stanz = new Array();
      const linebreak = [];
      for(let i = 0; i < stanzas; i++){
          stanz[i] = document.createElement("div1");
          linebreak[i] = document.createElement("br");
          for(let j = 0; j < 3; j++){
              //collect the lines of this stanze
              lines[i + j] = document.createElement("p");
              lines[i + j].innerHTML = poem[i][j];
              //add them to the stanza
              stanz[i].appendChild(lines[i + j]);
          }
          poBox.appendChild(stanz[i]);
          poBox.appendChild(linebreak[i]);
      }
}

function title(day, titleBank, poBox){
  var sign = document.createElement("p");
  var bold = document.createElement("b");
  let title = titleBank[day];
  bold.innerHTML = title;
  sign.appendChild(bold);
  poBox.appendChild(sign);
}

function author(day, authorBank, poBox){
  var sign = document.createElement("p");
  var bold = document.createElement("b");
  let writer =  "- " + authorBank[day];
  bold.innerHTML = writer;
  sign.appendChild(bold);
  poBox.appendChild(sign);
}
