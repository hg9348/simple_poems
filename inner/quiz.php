<?php

$script = $_SERVER['PHP_SELF'];
session_start();

if (!isset($_COOKIE['user'])) {
 header("location: ../tercetSignUp.php");
}
elseif (!isset($_SESSION['score'])) {
  $_SESSION["number"] = 0;
  $_SESSION["score"] = 0;
  header("Location: $script");
}
else { 
  $total_number = 6;
  $number = $_SESSION["number"];
  $score = $_SESSION["score"];
  
  if ($number == 0) {
    print <<<FIRST
<html lang="en">
<meta charset="utf-8">
<link rel="stylesheet" media="all" type="text/css" title="stylesheet" href="../tercet.css" />

<head>
  <title>Tercet Quiz</title>
</head>

<body>

  <img class="quizImg" alt="Quiz" src="./quiz.png" />
  <img id="crest" class="back" alt="Birds" src="../birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="../tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="$script">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./contact.html">Contact Us</a> </div>
    <div class="nav" id="Sign In"> <a href="../signIn.php">Sign In</a> </div>
  </div>

  <div class="content">
    <h2> Tercet Quiz </h2>
    <form method = "post" action = "$script">
    <p> 1) The term "tercet" means a set or group of three lines of verse. <br /><br /> 
    
    <input type="radio" name="q_one" value="q1true" id="q_one_true">
    <label for="q_one_true"> True </label>
  
    <input type="radio" name="q_one" value="q1false" id="q_one_false">
    <label for="q_one_false"> False </label>
    </p>
    <input type = "submit" value = "Next Question">
    </form>

    <div class="copyright">
    <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
      <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
    <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
        page.</a></span><br />
    <span> July 21, 2021, Wednesday </span>
    </div>
  </div>

</body>

</html>
  
FIRST;

  if (isset($_REQUEST['q_one'])) {
    $answer = $_POST['q_one'];
    if ($answer == 'q1true'){
      $score++;
      $number++;
      $_SESSION["number"] = $number;
      $_SESSION["score"] = $score;
    }
    else{
      $number++;
      $_SESSION["number"] = $number;
    }
    header("Location: $script");
  }
}

elseif ($number == 1) {
  print <<<SECOND
<html lang="en">
<meta charset="utf-8">
<link rel="stylesheet" media="all" type="text/css" title="stylesheet" href="../tercet.css" />

<head>
  <title>Tercet Quiz</title>
</head>

<body>

  <img class="quizImg" alt="Quiz" src="./quiz.png" />
  <img id="crest" class="back" alt="Birds" src="../birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="../tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="$script">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./contact.html">Contact Us</a> </div>
  </div>

  <div class="content">
    <h2> Tercet Quiz </h2>
  <form method = "post" action = "$script">
  <p> 2) What is the Japanese tercet form called? <br /><br /> 
  
  <div>
    <input type="checkbox" name="q_two" value="q2a" id="q_two_a">
    <label for="q_two_a"> a) arigatou </label>
  </div>

  <div>
    <input type="checkbox" name="q_two" value="q2b" id="q_two_b">
    <label for="q_two_b"> b) teriyaki </label>
  </div>

  <div>
    <input type="checkbox" name="q_two" value="q2c" id="q_two_c">
    <label for="q_two_c"> c) haiku </label>
  </div>

  <div>
    <input type="checkbox" name="q_two" value="q2d" id="q_two_d">
    <label for="q_two_d"> d) Doraemon </label>
  </div>
  </p>
  <input type = "submit" value = "Next Question">
  </form>

  <div class="copyright">
  <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
    <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
  <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
      page.</a></span><br />
  <span> July 21, 2021, Wednesday </span>
  </div>
</div>

</body>

</html>

SECOND;
  if (isset($_REQUEST['q_two'])) {
    $answer = $_POST['q_two'];
    if ($answer == 'q2c'){
      $score++;
      $number++;
      $_SESSION["number"] = $number;
      $_SESSION["score"] = $score;
    }
    else{
      $number++;
      $_SESSION["number"] = $number;
    }
    header("Location: $script");
  }
}

elseif ($number == 2) {
  print <<<THIRD
<html lang="en">
<meta charset="utf-8">
<link rel="stylesheet" media="all" type="text/css" title="stylesheet" href="../tercet.css" />

<head>
  <title>Tercet Quiz</title>
</head>

<body>

  <img class="quizImg" alt="Quiz" src="./quiz.png" />
  <img id="crest" class="back" alt="Birds" src="../birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="../tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="$script">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./contact.html">Contact Us</a> </div>
  </div>

  <div class="content">
    <h2> Tercet Quiz </h2>
    <form method = "post" action = "$script">
    <p> 3) The first occurrence of tercet in writing is in which of the following books?  <br /><br /> 
    
    <div>
      <input type="checkbox" name="q_three" value="q3a" id="q_three_a">
      <label for="q_three_a"> a) Hebrew Bible </label>
    </div>
  
    <div>
      <input type="checkbox" name="q_three" value="q3b" id="q_three_b">
      <label for="q_three_b"> b) Tao Te Ching </label>
    </div>
  
    <div>
      <input type="checkbox" name="q_three" value="q3c" id="q_three_c">
      <label for="q_three_c"> c) Bhagavad Gita </label>
    </div>
  
    <div>
      <input type="checkbox" name="q_three" value="q3d" id="q_three_d">
      <label for="q_three_d"> d) Old Testament </label>
    </div>
    </p>
    <input type = "submit" value = "Next Question">
    </form>

  <div class="copyright">
  <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
    <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
  <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
      page.</a></span><br />
  <span> July 21, 2021, Wednesday </span>
  </div>
</div>

</body>

</html>

THIRD;
  if (isset($_REQUEST['q_three'])) {
    $answer = $_POST['q_three'];
    if ($answer == 'q3a'){
      $score++;
      $number++;
      $_SESSION["number"] = $number;
      $_SESSION["score"] = $score;
    }
    else{
      $number++;
      $_SESSION["number"] = $number;
    }
    header("Location: $script");
  }
}

elseif ($number == 3) {
  print <<<FOURTH
<html lang="en">
<meta charset="utf-8">
<link rel="stylesheet" media="all" type="text/css" title="stylesheet" href="../tercet.css" />

<head>
  <title>Tercet Quiz</title>
</head>

<body>

  <img class="quizImg" alt="Quiz" src="./quiz.png" />
  <img id="crest" class="back" alt="Birds" src="../birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="../tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="$script">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./contact.html">Contact Us</a> </div>
  </div>

  <div class="content">
    <h2> Tercet Quiz </h2>
    <form method = "post" action = "$script">
    <p> 4) How many stanzas is in villanelle? <br /><br /> 
    
    <div>
      <input type="checkbox" name="q_four" value="q4a" id="q_four_a">
      <label for="q_four_a"> a) 1 </label>
    </div>
  
    <div>
      <input type="checkbox" name="q_four" value="q4b" id="q_four_b">
      <label for="q_four_b"> b) 2 </label>
    </div>
  
    <div>
      <input type="checkbox" name="q_four" value="q4c" id="q_four_c">
      <label for="q_four_c"> c) 6 </label>
    </div>
  
    <div>
      <input type="checkbox" name="q_four" value="q4d" id="q_four_d">
      <label for="q_four_d"> d) as many as you wish </label>
    </div>
    </p>
    <input type = "submit" value = "Next Question">
    </form>

  <div class="copyright">
  <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
    <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
  <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
      page.</a></span><br />
  <span> July 21, 2021, Wednesday </span>
  </div>
</div>

</body>

</html>

FOURTH;
  if (isset($_REQUEST['q_four'])) {
    $answer = $_POST['q_four'];
    if ($answer == 'q4d'){
      $score++;
      $number++;
      $_SESSION["number"] = $number;
      $_SESSION["score"] = $score;
    }
    else{
      $number++;
      $_SESSION["number"] = $number;
    }
    header("Location: $script");
  }
}

elseif ($number == 4) {
  print <<<FIFTH
<html lang="en">
<meta charset="utf-8">
<link rel="stylesheet" media="all" type="text/css" title="stylesheet" href="../tercet.css" />

<head>
  <title>Tercet Quiz</title>
</head>

<body>

  <img class="quizImg" alt="Quiz" src="./quiz.png" />
  <img id="crest" class="back" alt="Birds" src="../birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="../tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="$script">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./contact.html">Contact Us</a> </div>
  </div>

  <div class="content">
    <h2> Tercet Quiz </h2>
    <form method = "post" action = "$script">
    <p> 5) <input type="text" name="q_five" placeholder="enter a number"> syllables are in a single line of Sicilian Tercet. </p>
    <input type = "submit" value = "Next Question">
    </form>

  <div class="copyright">
  <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
    <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
  <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
      page.</a></span><br />
  <span> July 21, 2021, Wednesday </span>
  </div>
</div>

</body>

</html>

FIFTH;
  if (isset($_REQUEST['q_five'])) {
    $answer = $_POST['q_five'];
    $answer = trim($answer);
    $regex = '/ten/i';
    if ((preg_match($regex, $answer)) || ($answer == '10')) {
      $score++;
      $number++;
      $_SESSION["number"] = $number;
      $_SESSION["score"] = $score;
    }
    else{
      $number++;
      $_SESSION["number"] = $number;
    }
    header("Location: $script");
  }
}

elseif ($number == 5) {
  print <<<SIXTH
<html lang="en">
<meta charset="utf-8">
<link rel="stylesheet" media="all" type="text/css" title="stylesheet" href="../tercet.css" />

<head>
  <title>Tercet Quiz</title>
</head>

<body>

  <img class="quizImg" alt="Quiz" src="./quiz.png" />
  <img id="crest" class="back" alt="Birds" src="../birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="../tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="$script">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./contact.html">Contact Us</a> </div>
  </div>

  <div class="content">
    <h2> Tercet Quiz </h2>
    <form method = "post" action = "$script">
    <p> 6) Sir <input type="text" name="q_six" placeholder="enter a name"> first introduced tercet in English. </p>
    <input type = "submit" value = "Next Question">
    </form>

  <div class="copyright">
  <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
    <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
  <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
      page.</a></span><br />
  <span> July 21, 2021, Wednesday </span>
  </div>
</div>

</body>

</html>

SIXTH;
  if (isset($_REQUEST['q_six'])) {
    $answer = $_POST['q_six'];
    $answer = trim($answer);
    $regex = '/thomas wyatt/i';
    if (preg_match($regex, $answer)) {
      $score++;
      $number++;
      $_SESSION["number"] = $number;
      $_SESSION["score"] = $score;
    }
    else{
      $number++;
      $_SESSION["number"] = $number;
    }
    header("Location: $script");
  }
}

elseif ($number >= $total_number) {
  print <<<FINAL_SCORE
<html lang="en">
<meta charset="utf-8">
<link rel="stylesheet" media="all" type="text/css" title="stylesheet" href="../tercet.css" />

<head>
  <title>Tercet Quiz</title>
</head>

<body>

  <img class="quizImg" alt="Quiz" src="./quiz.png" />
  <img id="crest" class="back" alt="Birds" src="../birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="../tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="$script">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./contact.html">Contact Us</a> </div>
  </div>

  <div class="content">
  <p> Your final score is $score / $total_number. <br /><br />
FINAL_SCORE;
  if ($score > 3) {
    print <<<GOOD
  Good job!
  <div class="copyright">
  <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
    <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
  <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
      page.</a></span><br />
  <span> July 21, 2021, Wednesday </span>
  </div>
</div>

</body>

</html>
GOOD;
  }
  else {
    print <<<OKAY
  Thank you for playing!
  <div class="copyright">
  <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
    <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
  <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
      page.</a></span><br />
  <span> July 21, 2021, Wednesday </span>
  </div>
</div>

</body>

</html>
OKAY;
  }
  session_destroy();
}
}
?>
