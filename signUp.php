 <?php

$script = htmlspecialchars( $_SERVER['PHP_SELF']);

    if(!isset($_COOKIE['user'])){
        if(isset($_POST['signIn'])){
                setcookie("user", "logged in", time() + 3600*24);
                $table = "tercet";
                $host = "summer-2021.cs.utexas.edu";
                $user = "cs329e_mitra_hguo";
                $pwd = "Sample=plug+Every";
                $dbs = "cs329e_mitra_hguo";
                $port = "3306";
                $username = $_POST['user'];
                $password = $_POST['pass1'];
                $password = crypt($password);
                 $connect = mysqli_connect ($host, $user, $pwd, $dbs, $port);
                 $stmt = mysqli_prepare ($connect, "INSERT INTO $table VALUES (?, ?)");
                 mysqli_stmt_bind_param ($stmt, 'ss', $username, $password);
                 mysqli_stmt_execute($stmt);
                    mysqli_stmt_close($stmt);
                    mysqli_close($connect);
                    welcomePage();
            }
            else{
                    signUpPage();
            }}
    else{
        welcomePage();
    }

function welcomePage(){
    print <<< WELCOME

<html lang = "en">
<meta charset="utf-8">

        <head>  <title>Three Lines</title>
      <link rel = "stylesheet" href="./tercet.css" />
      </head>
      <body>

  <img class="logoImg" alt="Three Lines" src="./three_lin.png" />
  <img id="crest" class="back" alt="Birds" src="./birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="./tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./inner/poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./inner/history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./inner/howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="./inner/quiz.php">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./inner/contact.html">Contact Us</a> </div>
    <div class="nav" id="Sign In"> <a href="./signIn.php">Sign In</a> </div>
  </div>
  <div class="content">
<h2>Thank you for your membership with three lines!</h2>
  <footer class="copyrightMain">
    <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
      <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
    <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
        page.</a></span><br />
    <span> July 21, 2021, Wednesday </span>
  </footer>
</div>
</body>
</html>
WELCOME;
}



function signUpPage(){
    $script = htmlspecialchars( $_SERVER['PHP_SELF']);
print <<< SIGNIN
<html lang = "en">
<meta charset="utf-8">

        <head>  <title>Three Lines</title>
      <link rel = "stylesheet" href="./tercet.css" />
      </head>
      <body>
 
 <img class="logoImg" alt="Three Lines" src="./three_lin.png" />
  <img id="crest" class="back" alt="Birds" src="./birds.png" />

  <div class="navBar">
    <div class="nav" id="Home"> <a href="./tercetHome.html">Home</a> </div>
    <div class="nav" id="Poems"> <a href="./inner/poems.html">Poems</a> </div>
    <div class="nav" id="History"> <a href="./inner/history.html">History</a> </div>
    <div class="nav" id="How-To"> <a href="./inner/howto.html">How-To</a> </div>
    <div class="nav" id="Quiz"> <a href="./inner/quiz.php">Quiz</a> </div>
    <div class="nav" id="Contact"> <a href="./inner/contact.html">Contact Us</a> </div>
    <div class="nav" id="Sign In"> <a href="./signIn.php">Sign In</a> </div>
  </div>

  <div class="content">
<form method = "POST" action = "$script" id = "signUpInfo" >
<h4> Member Registration </h4>
<h6>Please make sure to meet the following requirements:</h6>
<ul>
<li>Username and Password must be between 6 and 16 characters long</li>
<li>Username and Password must be strictly alphanumeric and may contain no special characters</li>
<li>Both passwords must match</li>
</ul>
<table>
<tr>
<td><label>User Name</label></td>
<td><input type = "text" name = "user" value = "" id = "user" />
</tr>
<tr>
<td><label>Password</label></td>
<td><input type = "password" value = "" name = "pass1" id = "password1" /> </td>
</tr>
<tr>
<td><label>Repeat Password</label></td>
<td><input type = "password" value = "" name = "pass2" id = "password2" /> </td>
</tr>
<tr><td><br /></td></tr>
<tr>
<td><input type = "submit" value = "Sign Up" name = "signIn"  id = "Submit" /></td>
<td><input type = "reset" value = "Reset" /> </td>
</tr>
</table>
</form>
<script src = "./sha.js"></script>
<script>
const subBut = document.getElementById('Submit').value;
const scrip = document.createElement('script');


if (subBut == "Sign Up"){
        var firm = document.getElementById('signUpInfo');
        console.log("registration title loaded");

        firm.addEventListener('submit', (e) => {
                var usr = document.getElementById('user').value;
                var pass1 = document.getElementById('password1').value;
                var pass2 = document.getElementById('password2').value;
                if (pass1 != pass2){
                        window.alert("PLEASE MAKE SURE BOTH PASSWORDS MATCH");
                        e.preventDefault();
                }

                if (usr == null || pass1 == null || pass2 == null){
                        window.alert("PLEASE MAKE SURE USERNAME/PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (usr.length > 16 || usr.length < 6){
                        window.alert("PLEASE MAKE SURE USERNAME IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (pass1.length > 16 || pass1.length < 6){
                        window.alert("PLEASE MAKE SURE PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }

                var num = /\D/;
                var alph = /\W/;
                if (num.test(pass1) && alph.test(pass1)){
                        window.alert("PLEASE MAKE SURE PASSWORD CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }

                if (num.test(usr) && alph.test(usr)){
                        window.alert("PLEASE MAKE SURE USERNAME CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }
                document.getElementById('password1').value = Sha256.hash(pass1);
        })

}


if (subBut == "Log in"){ 
        var farm = document.getElementById('loginInfo');
        console.log("login title loaded");

        farm.addEventListener('submit', (e) => {
                var usr = document.getElementById('user').value;
                var pass = document.getElementById('password').value;
                if (usr == null || pass == null){
                        window.alert("PLEASE MAKE SURE USERNAME/PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (usr.length > 16 || usr.length < 6){
                        window.alert("PLEASE MAKE SURE USERNAME IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }
                if (pass.length > 16 || pass.length < 6){
                        window.alert("PLEASE MAKE SURE PASSWORD IS BETWEEN 6 AND 16 CHARACTERS LONG");
                        e.preventDefault();
                }

                var num = /\D/;
                var alph = /\W/;
                if (num.test(pass) && alph.test(pass)){
                        window.alert("PLEASE MAKE SURE PASSWORD CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }

                if (num.test(usr) && alph.test(usr)){
                        window.alert("PLEASE MAKE SURE USERNAME CONTAINS ONLY LETTERS AND NUMBERS");
                        e.preventDefault();
                }
                document.getElementById('password').value = Sha256.hash(pass);
        })
}
  

</script>

  <footer class="copyrightMain">
    <span> © 2021 <a href="mailto:casehartsough@utexas.edu"> Casey Hartsough</a>,
      <a href="mailto:hguo@cs.utexas.edu"> Haohang Guo </a></span><br />
    <span> For questions, feedback, and information on the authors, see our <a href="./tercetContact.html">Contact
        page.</a></span><br />
    <span> July 21, 2021, Wednesday </span>
  </footer>
</div>
</body>
</html>
SIGNIN;
}

?>
